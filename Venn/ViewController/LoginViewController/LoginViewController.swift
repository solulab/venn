//
//  LoginViewController.swift
//  Venn
//
//  Created by Hetal Govani on 21/11/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    open override var shouldAutorotate: Bool
    {
        return true
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.isHidden = true
    }
    @IBAction func btnStudentPress(sender:UIButton)
    {
        let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "LoginStudentViewController") as! LoginStudentViewController
        self.navigationController?.pushViewController(viewObj, animated: true)
    }
  
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
