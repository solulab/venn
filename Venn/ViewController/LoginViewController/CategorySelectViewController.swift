//
//  CategorySelectViewController.swift
//  Venn
//
//  Created by Hetal Govani on 24/11/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
import JCTagListView

class CategorySelectViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet var collectionView : UICollectionView!

    @IBOutlet var viewForYou : UIView!
    @IBOutlet var tagListView: JCTagListView!
    
    var arrMainCategory : Array<String>! = Array()
    var arrAll : Array<String>! = Array()
    var arrMaths : Array<String>! = Array()
    var arrScience : Array<String>! = Array()
    var arrHistory : Array<String>! = Array()
    var arrEnglish : Array<String>! = Array()
    var arrTest : Array<String>! = Array()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let button1 = UIButton(type: UIButtonType.custom)
        button1.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        button1.frame=CGRect.init(x: 0, y: 0, width:30, height: 30)
        let barButton1 = UIBarButtonItem(customView: button1)
        self.navigationItem.leftBarButtonItem = barButton1

//        let btnright1 = UIButton(type: UIButtonType.custom)
//        btnright1.setImage(#imageLiteral(resourceName: "student_small"), for: UIControlState.normal)
//        btnright1.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
//        let barbtnright1 = UIBarButtonItem(customView: btnright1)
//        self.navigationItem.rightBarButtonItem = barbtnright1

        arrAll = ["Algebra","Geometry","Calculus","Number Theory","Logic","Combinatorics","Physics","Chemistry","Biology", "Zoology","Botany","Geology","Astronomy","Ecology","Psychology","American","International","War","Ancient", "World Wars","European","Asian","Vocabulary","Grammer","Speaking","Writing","Listening","Reasoning","SAT","GMAT", "IELTS","GRE","TOEFL"]
        
        arrMaths = ["Algebra","Geometry","Calculus","Number Theory", "Logic","Combinatorics"]
        
        arrScience = ["Physics","Chemistry","Biology","Zoology","Botany","Geology","Astronomy","Ecology","Psychology"]
        
        arrHistory = ["American","International","War","Ancient","World Wars","European","Asian"]
        
        arrEnglish = ["Vocabulary","Grammer","Speaking","Writing","Listening","Reasoning"]
        
        arrTest = ["SAT","GMAT","IELTS","GRE","TOEFL"]
        
        self.tagListView.tags = NSMutableArray(array: arrAll)
        self.tagListView.selectedTags = ["Calculus"]

        arrMainCategory = ["ALL","MATHS","SCIENCE","HISTORY","ENGLISH","TEST PREPARATIION"]
    }
    open override var shouldAutorotate: Bool
    {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if indexPath.row == 5 {
            return CGSize.init(width: 160, height: 40)
        }
        return CGSize.init(width: 80, height: 40)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! mainCategoryCell
        cell.lblName.text = arrMainCategory[indexPath.row]
        cell.lblMark.isHidden = true

        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = self.collectionView.cellForItem(at: indexPath) as! mainCategoryCell
        cell.lblName.textColor = PINKCOLOR
        cell.lblMark.isHidden = false
        if indexPath.row == 0
        {
           
            self.tagListView.tags = NSMutableArray(array: arrAll)
        }
        else if indexPath.row == 1
        {
            self.tagListView.tags = NSMutableArray(array: arrMaths)
        }
        else if indexPath.row == 2
        {
            self.tagListView.tags = NSMutableArray(array: arrScience)
        }
        else if indexPath.row == 3
        {
            self.tagListView.tags = NSMutableArray(array: arrHistory)
        }
        else if indexPath.row == 4
        {
            self.tagListView.tags = NSMutableArray(array: arrEnglish)
        }
        else if indexPath.row == 5
        {
            self.tagListView.tags = NSMutableArray(array: arrTest)
        }
        tagListView.collectionView.reloadData()
    }
   
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = self.collectionView.cellForItem(at: indexPath) as! mainCategoryCell
        cell.lblName.textColor = GRAYCOLOR
        cell.lblMark.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnGoToVennPress(sender:UIButton)
    {
        let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        viewObj.arrCategories = tagListView.selectedTags as NSArray as! [String]
        self.navigationController?.pushViewController(viewObj, animated: true)
    }
}
class mainCategoryCell : UICollectionViewCell
{
    @IBOutlet var lblName : UILabel!
    @IBOutlet var lblMark : UILabel!
}
