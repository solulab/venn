//
//  LoginStudentViewController.swift
//  Venn
//
//  Created by Hetal Govani on 21/11/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit

class LoginStudentViewController: UIViewController {

    @IBOutlet var viewHeight : NSLayoutConstraint!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false

        viewHeight.constant = self.view.frame.height - 64
        
        let button1 = UIButton(type: UIButtonType.custom)
        button1.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        button1.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barButton1 = UIBarButtonItem(customView: button1)
        self.navigationItem.leftBarButtonItem = barButton1
        
        let viewTitle = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 90, height: 40))
        let imgView = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: 90, height: 40))
        imgView.image = #imageLiteral(resourceName: "logo_black")
        imgView.contentMode = .scaleAspectFit
        viewTitle.addSubview(imgView)
//        self.navigationItem.titleView?.clipsToBounds = true
        self.navigationItem.titleView = viewTitle
    }
    open override var shouldAutorotate: Bool
    {
        return true
    }
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnGoToVennPress(sender:UIButton)
    {
        let categorySelectionVC = self.storyboard?.instantiateViewController(withIdentifier: "CategorySelectViewController") as! CategorySelectViewController
        self.navigationController?.pushViewController(categorySelectionVC, animated: true)
    }
    @IBAction func btnSignUpPress(sender:UIButton)
    {
        let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(viewObj, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
