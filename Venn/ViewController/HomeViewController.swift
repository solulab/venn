//
//  HomeViewController.swift
//  Venn
//
//  Created by Hetal Govani on 22/11/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
import JCTagListView

class HomeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    @IBOutlet var viewForYou : UIView!
    var arrDescription : Array<String>! = Array()
    var arrTitle : Array<String>! = Array()
    var arrImage : Array<UIImage>! = Array()
    var arrStatus : Array<Int>! = Array()
    var arrPrice : Array<Int>! = Array()
    var arrStart : Array<String>! = Array()
    var arrRateView : Array<Float>! = Array()
    var arrRate : Array<String>! = Array()
    var arrCategories : Array<String>! = Array()
    @IBOutlet var collectionView: UICollectionView!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let button1 = UIButton(type: UIButtonType.custom)
        button1.setImage(#imageLiteral(resourceName: "logo_small"), for: UIControlState.normal)
        button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        button1.frame=CGRect.init(x: 0, y: 0, width: 90, height: 40)
        let barButton1 = UIBarButtonItem(customView: button1)
        self.navigationItem.leftBarButtonItem = barButton1
        
        let btnright1 = UIButton(type: UIButtonType.custom)
        btnright1.setImage(#imageLiteral(resourceName: "broadcast_light"), for: UIControlState.normal)
        //        button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        btnright1.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barbtnright1 = UIBarButtonItem(customView: btnright1)
        
        let btnright2 = UIButton(type: UIButtonType.custom)
        btnright2.setImage(#imageLiteral(resourceName: "search"), for: UIControlState.normal)
        //        button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        btnright2.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barbtnright2 = UIBarButtonItem(customView: btnright2)
        
        let btnright3 = UIButton(type: UIButtonType.custom)
        btnright3.setImage(#imageLiteral(resourceName: "student_pink"), for: UIControlState.normal)
        btnright3.addTarget(self, action:#selector(btnProfilePress(sender:)), for: UIControlEvents.touchUpInside)
        btnright3.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barbtnright3 = UIBarButtonItem(customView: btnright3)
        self.navigationItem.rightBarButtonItems = [barbtnright3,barbtnright2,barbtnright1]
        
        viewForYou.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.7).cgColor
        viewForYou.layer.borderWidth = 0.5
        
        arrStatus = [1,2,3,4,1,2,3,4,1,2,3]
        
        arrImage = [#imageLiteral(resourceName: "profile"),#imageLiteral(resourceName: "profile pic1"),#imageLiteral(resourceName: "profile pic2"),#imageLiteral(resourceName: "profile pic3"),#imageLiteral(resourceName: "profile pic4"),#imageLiteral(resourceName: "profile pic5"),#imageLiteral(resourceName: "profile pic6"),#imageLiteral(resourceName: "profile pic7"),#imageLiteral(resourceName: "profile pic8"),#imageLiteral(resourceName: "profile pic9"),#imageLiteral(resourceName: "profile pic10")]
        
        arrPrice = [60,20,40,54,90,80,70,25,30,45,90]
        
        arrRateView = [2.0,3.0,4.0,2.5,4.5,4.0,3.5,2.0,3.0,4.0,3.0]
        arrRate = ["2.0(430)","3.0(150)","4.0(500)","2.5(330)","4.5(250)","4.0(300)","3.5(600)","2.0(750)","3.0(700)","4.0(400)","3.0(200)"]

        arrStart = ["Start","Book","Start","Book","Book","Start","Book","Book","Start","Book","Book","Start"]
        arrTitle = ["Michael N.",
            "Rajnish R.",
            "Parmanand K.",
            "Abhay p.",
            "YASHVI U.",
            "Khyati B.",
            "Dr. Vikrant S.",
            "Gajanan P.",
            "Nandini A.",
            "Rythm G.",
            "Anytime S."]
        
        arrDescription = ["English/Writing/Reading/Study Habits Tutor",
            "math, Algebra 1, Algebra 2, AP Computer Science",
            "CLEP Calculus, Calculus 3, Calculus 2, Calculus 1, C++, Business Statistics, Business",
            "Pre-Algebra, Pre-Calculus, PSAT Mathematics, Quantitative Reasoning, SAT Math, SAT Mathematics",
            "Geometry, Trigonometry, Organic Chemistry, Chemistry, 11th Grade math",
            "SAT, MCAT, DAT, UKCAT, UMAT, GMSAT, ASVAB, GED, Praxis, PSAT, SAT",
            "Physiology, MCAT Biological Sciences, Life Sciences, GRE Subject Test in Biology, Zoology, Ecology, Biotechnology, Biochemistry, Medicine",
            " GRE Verbal Reasoning, GRE Verbal, GRE, Grammar and Mechanics, GMAT Verbal, GMAT",
            "* Corporate work experience * OOPS concepts * HTML , CSS * SQL Server 2005, 2008 , 2010 including stored procedures * .NET ",
            "Agricultural Science, ACT Science, 1st Grade Writing, AP Biology, ACT English, American Literature",
            "Physics 1 - DUPE, AP Physics 2 - DUPE, AP Physics 2: Electricity and Magnetism, AP Physics 2: Mechanics"]
        
        print("--------\(arrCategories!)------")
       
    }
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(true)
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.enableAllOrientation = false
//    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        
        self.navigationController?.navigationBar.isHidden = false
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.enableAllOrientation = false
        
        let value = UIInterfaceOrientation.portrait.rawValue;
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    @IBAction func btnProfilePress(sender:UIButton)
    {
        let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "StudentProfileViewController") as! StudentProfileViewController
        self.navigationController?.pushViewController(viewObj, animated: true)
    }
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    open override var shouldAutorotate: Bool
    {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrCategories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let str = arrCategories[indexPath.row]
        return CGSize.init(width: str.width(withConstraintedHeight: 35) + 5, height: 35)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! mainCategoryCell
        cell.lblName.text = arrCategories[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 11
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! homeCell
        cell.selectionStyle = .none
        cell.lblOnline.layer.cornerRadius = 4.0
        cell.imgView.layer.cornerRadius = 30
        cell.btnStart.layer.cornerRadius = 5
        
        cell.lblName.text = "\(arrTitle[indexPath.row])"
        cell.lblDescription.text = "\(arrDescription[indexPath.row])"
        cell.imgView.image = arrImage[indexPath.row]
        cell.btnStart.setTitle(arrStart[indexPath.row], for: .normal)
        cell.btnStart.addTarget(self, action: #selector(btnStartPress), for: .touchUpInside)
        cell.btnStart.tag = indexPath.row
        cell.lblPrice.text = "$\(arrPrice[indexPath.row])/hour"
        
        cell.rateView.rating = arrRateView[indexPath.row]
        cell.lblRate.text = "\(arrRate[indexPath.row])"

        if arrStart[indexPath.row] == "Book"
        {
            cell.btnStart.setTitleColor(PINKCOLOR, for: .normal)
            cell.btnStart.layer.borderWidth = 1
            cell.btnStart.layer.borderColor = PINKCOLOR.cgColor
            cell.btnStart.backgroundColor = UIColor.white
        }
        else
        {
            cell.btnStart.setTitleColor(UIColor.white, for: .normal)
            cell.btnStart.layer.borderWidth = 1
            cell.btnStart.layer.borderColor = PINKCOLOR.cgColor
            cell.btnStart.backgroundColor = PINKCOLOR
        }
        
        if indexPath.row == 3 || indexPath.row == 5 || indexPath.row == 8
        {
            cell.lblPro.isHidden = false
        }
        else
        {
            cell.lblPro.isHidden = true
        }
        let status = arrStatus[indexPath.row]
        
        if status == 1
        {
           cell.lblOnline.backgroundColor = UIColor.green
        }
        else if status == 2
        {
            cell.lblOnline.backgroundColor = UIColor.lightGray
        }
        else if status == 3
        {
            cell.lblOnline.backgroundColor = UIColor.yellow
        }
        if status == 4
        {
            cell.lblOnline.backgroundColor = UIColor.red
        }
        return cell
    }
    
    @IBAction func btnStartPress(sender:UIButton)
    {
        if arrStart[sender.tag] == "Start"
        {
            let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "VideoChatViewController") as! VideoChatViewController
            self.navigationController?.pushViewController(viewObj, animated: true)
        }
        else
        {
            let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "ScheduleViewController") as! ScheduleViewController
            self.navigationController?.pushViewController(viewObj, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "TutorDetailViewController") as! TutorDetailViewController
        self.navigationController?.pushViewController(viewObj, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
class homeCell: UITableViewCell {
    @IBOutlet var btnStart : UIButton!
    @IBOutlet var imgView : UIImageView!
    @IBOutlet var lblOnline : UILabel!
    @IBOutlet var lblName : UILabel!
    @IBOutlet var lblDescription : UILabel!
    @IBOutlet var lblPrice : UILabel!
    @IBOutlet var rateView : FloatRatingView!
    @IBOutlet var lblRate : UILabel!
    @IBOutlet var lblPro : UILabel!
}
