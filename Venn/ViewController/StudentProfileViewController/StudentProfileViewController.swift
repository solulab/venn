//
//  StudentProfileViewController.swift
//  Venn
//
//  Created by Deep Sheth on 12/12/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
import JCTagListView

class StudentProfileViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    @IBOutlet var scrViewProfile : UIScrollView!
    @IBOutlet var scrViewChangePassword : UIScrollView!
    @IBOutlet var viewSetting : UIView!

    @IBOutlet var imgProfile : UIImageView!
    @IBOutlet var lblProfile : UILabel!
    @IBOutlet var imgbottomProfile : UIImageView!

    @IBOutlet var imgChangePassword : UIImageView!
    @IBOutlet var lblChangePassword : UILabel!
    @IBOutlet var imgbottomChangePassword : UIImageView!

    @IBOutlet var imgSetting : UIImageView!
    @IBOutlet var lblSetting : UILabel!
    @IBOutlet var imgbottomSetting : UIImageView!

    @IBOutlet var collectionView : UICollectionView!

    @IBOutlet var tagListView: JCTagListView!
    
    var arrMainCategory : Array<String>! = Array()
    var arrAll : Array<String>! = Array()
    var arrMaths : Array<String>! = Array()
    var arrScience : Array<String>! = Array()
    var arrHistory : Array<String>! = Array()
    var arrEnglish : Array<String>! = Array()
    var arrTest : Array<String>! = Array()

    @IBOutlet var viewHeightPassword : NSLayoutConstraint!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.title = "My Account"
        
    //    viewHeightPassword.constant = self.view.frame.height - (64+70)
        
        let button1 = UIButton(type: UIButtonType.custom)
        button1.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        button1.frame=CGRect.init(x: 0, y: 0, width: 30, height:30)
        let barButton1 = UIBarButtonItem(customView: button1)
        self.navigationItem.leftBarButtonItem = barButton1
        
        let btnright1 = UIButton(type: UIButtonType.custom)
        btnright1.setImage(#imageLiteral(resourceName: "broadcast_light"), for: UIControlState.normal)
        //        button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        btnright1.frame=CGRect.init(x: 0, y: 0, width: 25, height: 30)
        let barbtnright1 = UIBarButtonItem(customView: btnright1)
        
        let btnright2 = UIButton(type: UIButtonType.custom)
        btnright2.setImage(#imageLiteral(resourceName: "search"), for: UIControlState.normal)
        //        button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        btnright2.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barbtnright2 = UIBarButtonItem(customView: btnright2)

        self.navigationItem.rightBarButtonItems = [barbtnright2,barbtnright1]
        
        btnProfilePress(sender: UIButton())
        
        arrAll = ["Algebra","Geometry","Calculus","Number Theory","Logic","Combinatorics","Physics","Chemistry","Biology", "Zoology","Botany","Geology","Astronomy","Ecology","Psychology","American","International","War","Ancient", "World Wars","European","Asian","Vocabulary","Grammer","Speaking","Writing","Listening","Reasoning","SAT","GMAT", "IELTS","GRE","TOEFL"]
        
        arrMaths = ["Algebra","Geometry","Calculus","Number Theory", "Logic","Combinatorics"]
        
        arrScience = ["Physics","Chemistry","Biology","Zoology","Botany","Geology","Astronomy","Ecology","Psychology"]
        
        arrHistory = ["American","International","War","Ancient","World Wars","European","Asian"]
        
        arrEnglish = ["Vocabulary","Grammer","Speaking","Writing","Listening","Reasoning"]
        
        arrTest = ["SAT","GMAT","IELTS","GRE","TOEFL"]
        
        self.tagListView.tags = NSMutableArray(array: arrAll)
        self.tagListView.selectedTags = ["Calculus"]

        arrMainCategory = ["ALL","MATHS","SCIENCE","HISTORY","ENGLISH","TEST PREPARATIION"]
        
        collectionView.reloadData()
    }
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnProfilePress(sender:UIButton)
    {
        scrViewProfile.isHidden = false
        viewSetting.isHidden = true
        scrViewChangePassword.isHidden = true
        
        imgProfile.image = #imageLiteral(resourceName: "aboutTutor_pink")
        lblProfile.textColor = PINKCOLOR
        
        imgSetting.image = #imageLiteral(resourceName: "setting")
        lblSetting.textColor = GRAYCOLOR
        
        imgChangePassword.image = #imageLiteral(resourceName: "key")
        lblChangePassword.textColor = GRAYCOLOR
        
        imgbottomProfile.isHidden = false
        imgbottomChangePassword.isHidden = true
        imgbottomSetting.isHidden = true
    }
    @IBAction func btnChangePasswordPress(sender:UIButton)
    {
        scrViewProfile.isHidden = true
        viewSetting.isHidden = true
        scrViewChangePassword.isHidden = false
        
        imgProfile.image = #imageLiteral(resourceName: "aboutTutor")
        lblProfile.textColor = GRAYCOLOR
        
        imgSetting.image = #imageLiteral(resourceName: "setting")
        lblSetting.textColor = GRAYCOLOR
        
        imgChangePassword.image = #imageLiteral(resourceName: "key_pink")
        lblChangePassword.textColor = PINKCOLOR
        
        imgbottomProfile.isHidden = true
        imgbottomChangePassword.isHidden = false
        imgbottomSetting.isHidden = true
    }
    @IBAction func btnSettingPress(sender:UIButton)
    {
        scrViewProfile.isHidden = true
        viewSetting.isHidden = false
        scrViewChangePassword.isHidden = true
        
        imgProfile.image = #imageLiteral(resourceName: "aboutTutor")
        lblProfile.textColor = GRAYCOLOR
        
        imgSetting.image = #imageLiteral(resourceName: "setting_pink")
        lblSetting.textColor = PINKCOLOR
        
        imgChangePassword.image = #imageLiteral(resourceName: "key")
        lblChangePassword.textColor = GRAYCOLOR
        
        imgbottomProfile.isHidden = true
        imgbottomChangePassword.isHidden = true
        imgbottomSetting.isHidden = false
        tagListView.collectionView.reloadData()

        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if indexPath.row == 5 {
            return CGSize.init(width: 160, height: 40)
        }
        return CGSize.init(width: 80, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! mainCategoryCell
        cell.lblName.text = arrMainCategory[indexPath.row]
        cell.lblMark.isHidden = true
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = self.collectionView.cellForItem(at: indexPath) as! mainCategoryCell
        cell.lblName.textColor = PINKCOLOR
        cell.lblMark.isHidden = false
        if indexPath.row == 0
        {
            self.tagListView.tags = NSMutableArray(array: arrAll)
        }
        else if indexPath.row == 1
        {
            self.tagListView.tags = NSMutableArray(array: arrMaths)
        }
        else if indexPath.row == 2
        {
            self.tagListView.tags = NSMutableArray(array: arrScience)
        }
        else if indexPath.row == 3
        {
            self.tagListView.tags = NSMutableArray(array: arrHistory)
        }
        else if indexPath.row == 4
        {
            self.tagListView.tags = NSMutableArray(array: arrEnglish)
        }
        else if indexPath.row == 5
        {
            self.tagListView.tags = NSMutableArray(array: arrTest)
        }
        tagListView.collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let cell = self.collectionView.cellForItem(at: indexPath) as? mainCategoryCell
        {
            cell.lblName.textColor = GRAYCOLOR
            cell.lblMark.isHidden = true
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
