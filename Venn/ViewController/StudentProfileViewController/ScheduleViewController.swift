
//
//  ScheduleViewController.swift
//  Venn
//
//  Created by Hetal Govani on 13/12/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit

class ScheduleViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout ,SCPopDatePickerDelegate
{
    var arrTime : Array<String>! = Array()
    let datePicker = SCPopDatePicker()
    @IBOutlet var lblDate : UILabel!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.title = "My Account"

        let button1 = UIButton(type: UIButtonType.custom)
        button1.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        button1.frame=CGRect.init(x: 0, y: 0, width: 30, height:30)
        let barButton1 = UIBarButtonItem(customView: button1)
        self.navigationItem.leftBarButtonItem = barButton1
        
        let btnright1 = UIButton(type: UIButtonType.custom)
        btnright1.setImage(#imageLiteral(resourceName: "broadcast_light"), for: UIControlState.normal)
        //        button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        btnright1.frame=CGRect.init(x: 0, y: 0, width: 25, height: 30)
        let barbtnright1 = UIBarButtonItem(customView: btnright1)
        
        let btnright2 = UIButton(type: UIButtonType.custom)
        btnright2.setImage(#imageLiteral(resourceName: "search"), for: UIControlState.normal)
        //        button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        btnright2.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barbtnright2 = UIBarButtonItem(customView: btnright2)
        
        let btnright3 = UIButton(type: UIButtonType.custom)
        btnright3.setImage(#imageLiteral(resourceName: "student_pink"), for: UIControlState.normal)
        btnright3.addTarget(self, action:#selector(btnProfilePress(sender:)), for: UIControlEvents.touchUpInside)
        btnright3.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barbtnright3 = UIBarButtonItem(customView: btnright3)
        self.navigationItem.rightBarButtonItems = [barbtnright3,barbtnright2,barbtnright1]
        arrTime = ["08:00 AM","02:00 PM","08:00 PM","09:00 AM","03:00 PM","09:00 PM","10:00 AM","04:00 PM","10:00 PM","11:00 AM","05:00 PM","11:00 PM","12:00 AM","06:00 PM","12:00 AM","01:00 PM","07:00 PM","01:00 AM"]
    }
    @IBAction func btnProfilePress(sender:UIButton)
    {
        let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "StudentProfileViewController") as! StudentProfileViewController
        self.navigationController?.pushViewController(viewObj, animated: true)
    }
    @IBAction func showSambagDatePickerViewController(_ sender: UIButton) {
        self.datePicker.tapToDismiss = true
        self.datePicker.datePickerType = SCDatePickerType.date
        self.datePicker.showBlur = true
        self.datePicker.datePickerStartDate = Date()
        self.datePicker.btnFontColour = UIColor.white
        self.datePicker.btnColour = PINKCOLOR
        self.datePicker.showCornerRadius = true
        self.datePicker.delegate = self
        self.datePicker.show(attachToView: self.view)
        
    }
    func scPopDatePickerDidSelectDate(_ date: Date) {
        print(date)
//        selectedDate = date
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        
        let year =  components.year
        let month = components.month
        let day = components.day
        
        print(year!)
        print(String(format: "%02d", month!))
        let strDay : String! = String(format: "%02d", day!)
        let strMonth : String! = String(format: "%02d", month!)
        let strYear : String! = String(year!)
        
        lblDate.text = "\(strDay!)/\(strMonth!)/\(strYear!)"
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrTime.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! timeCell
        cell.lblTime.text =  arrTime[indexPath.row]
        
        if indexPath.row == 5 || indexPath.row == 10
        {
            cell.isUserInteractionEnabled = false
            cell.backgroundColor = UIColor.lightGray
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
         return CGSize.init(width: (self.view.frame.width-30)/3, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row != 5 || indexPath.row != 10
        {
            let cell = collectionView.cellForItem(at: indexPath) as! timeCell
            cell.backgroundColor = PINKCOLOR
        }
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if indexPath.row != 5 || indexPath.row != 10
        {
            let cell = collectionView.cellForItem(at: indexPath) as! timeCell
            cell.backgroundColor = UIColor.init(red: 79/255, green: 88/255, blue: 95/255, alpha: 1.0)
        }//79 88 95
    }
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
class timeCell : UICollectionViewCell {
    @IBOutlet var lblTime : UILabel!
}
