//
//  TutorDetailViewController.swift
//  Venn
//
//  Created by Hetal Govani on 23/11/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit

class TutorDetailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,RateUsDelegate
{
    
    
    @IBOutlet var scrViewQualification : UIScrollView!
    @IBOutlet var scrViewAboutTutor : UIScrollView!
    @IBOutlet var viewRating : UIView!
    
    @IBOutlet var imgQualification : UIImageView!
    @IBOutlet var lblQualification : UILabel!
    @IBOutlet var imgbottomQualification : UIImageView!
    
    @IBOutlet var imgAboutTutor : UIImageView!
    @IBOutlet var lblAboutTutor : UILabel!
    @IBOutlet var imgbottomAboutTutor : UIImageView!
    
    @IBOutlet var imgRating : UIImageView!
    @IBOutlet var lblRating : UILabel!
    @IBOutlet var imgbottomRating : UIImageView!
    
    @IBOutlet var tblRating : UITableView!
    
    var arrImage : Array<UIImage>! = Array()
    @IBOutlet var viewBottomRating : UIView!
    @IBOutlet var viewBottomQualification : UIView!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let button1 = UIButton(type: UIButtonType.custom)
        button1.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        button1.frame=CGRect.init(x: 0, y: 0, width: 30, height: 40)
        let barButton1 = UIBarButtonItem(customView: button1)
        self.navigationItem.leftBarButtonItem = barButton1
        
        let btnright1 = UIButton(type: UIButtonType.custom)
        btnright1.setImage(#imageLiteral(resourceName: "broadcast_light"), for: UIControlState.normal)
        //        button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        btnright1.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barbtnright1 = UIBarButtonItem(customView: btnright1)
        
        let btnright2 = UIButton(type: UIButtonType.custom)
        btnright2.setImage(#imageLiteral(resourceName: "search"), for: UIControlState.normal)
        //        button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        btnright2.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barbtnright2 = UIBarButtonItem(customView: btnright2)
        
        let btnright3 = UIButton(type: UIButtonType.custom)
        btnright3.setImage(#imageLiteral(resourceName: "student_pink"), for: UIControlState.normal)
        btnright3.addTarget(self, action:#selector(btnProfilePress(sender:)), for: UIControlEvents.touchUpInside)
        btnright3.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barbtnright3 = UIBarButtonItem(customView: btnright3)
        self.navigationItem.rightBarButtonItems = [barbtnright3,barbtnright2,barbtnright1]
        
        arrImage = [#imageLiteral(resourceName: "profile"),#imageLiteral(resourceName: "profile pic1"),#imageLiteral(resourceName: "profile pic2"),#imageLiteral(resourceName: "profile pic3"),#imageLiteral(resourceName: "profile pic4"),#imageLiteral(resourceName: "profile pic5"),#imageLiteral(resourceName: "profile pic6"),#imageLiteral(resourceName: "profile pic7"),#imageLiteral(resourceName: "profile pic8"),#imageLiteral(resourceName: "profile pic9")]

        btnQualificationPress(sender: UIButton())
    }
   
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        
        self.navigationController?.navigationBar.isHidden = false
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.enableAllOrientation = false
        
        let value = UIInterfaceOrientation.portrait.rawValue;
        UIDevice.current.setValue(value, forKey: "orientation")
    }
    @IBAction func btnProfilePress(sender:UIButton)
    {
        let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "StudentProfileViewController") as! StudentProfileViewController
        self.navigationController?.pushViewController(viewObj, animated: true)
    }
    @IBAction func btnQualificationPress(sender:UIButton)
    {
        scrViewQualification.isHidden = false
        viewRating.isHidden = true
        scrViewAboutTutor.isHidden = true
        
        imgQualification.image = #imageLiteral(resourceName: "qualification_pink")
        lblQualification.textColor = PINKCOLOR
        
        imgRating.image = #imageLiteral(resourceName: "review")
        lblRating.textColor = GRAYCOLOR
        
        imgAboutTutor.image = #imageLiteral(resourceName: "aboutTutor")
        lblAboutTutor.textColor = GRAYCOLOR
        
        imgbottomQualification.isHidden = false
        imgbottomAboutTutor.isHidden = true
        imgbottomRating.isHidden = true
        
        viewBottomRating.isHidden = true
        viewBottomQualification.isHidden = false
    }
    @IBAction func btnAboutTutorPress(sender:UIButton)
    {
        scrViewQualification.isHidden = true
        viewRating.isHidden = true
        scrViewAboutTutor.isHidden = false
        
        imgQualification.image = #imageLiteral(resourceName: "qualification")
        lblQualification.textColor = GRAYCOLOR
        
        imgRating.image = #imageLiteral(resourceName: "review")
        lblRating.textColor = GRAYCOLOR
        
        imgAboutTutor.image = #imageLiteral(resourceName: "aboutTutor_pink")
        lblAboutTutor.textColor = PINKCOLOR
        
        imgbottomQualification.isHidden = true
        imgbottomAboutTutor.isHidden = false
        imgbottomRating.isHidden = true
        viewBottomRating.isHidden = true
        viewBottomQualification.isHidden = true
    }
    @IBAction func btnRatingPress(sender:UIButton)
    {
        scrViewQualification.isHidden = true
        viewRating.isHidden = false
        scrViewAboutTutor.isHidden = true
        
        imgQualification.image = #imageLiteral(resourceName: "qualification")
        lblQualification.textColor = GRAYCOLOR
        
        imgRating.image = #imageLiteral(resourceName: "review_pink")
        lblRating.textColor = PINKCOLOR
        
        imgAboutTutor.image = #imageLiteral(resourceName: "aboutTutor")
        lblAboutTutor.textColor = GRAYCOLOR
        
        imgbottomQualification.isHidden = true
        imgbottomAboutTutor.isHidden = true
        imgbottomRating.isHidden = false
        
        viewBottomRating.isHidden = false
        viewBottomQualification.isHidden = true
        
        tblRating.reloadData()
        
    }
    func SubmitButtonClicked(_ secondDetailViewController: RateViewController) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationFade)
        
    }
    
    func DismissButtonClicked(_ secondDetailViewController: RateViewController) {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationFade)
        
    }
    @IBAction func btnStartPress(sender:UIButton)
    {
        let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "VideoChatViewController") as! VideoChatViewController
        self.navigationController?.pushViewController(viewObj, animated: true)
    }
    @IBAction func btnSchedulePress(sender:UIButton)
    {
        let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "ScheduleViewController") as! ScheduleViewController
        self.navigationController?.pushViewController(viewObj, animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! rateCell
        cell.selectionStyle = .none
        cell.imgView.image = arrImage[indexPath.row]
        return cell
    }
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnWriteReviewPress(sender:UIButton)
    {
        let ratingVC = RateViewController(nibName: "RateViewController", bundle: nil)
        ratingVC.view.frame = CGRect.init(x: 10, y: ratingVC.view.frame.origin.y, width: self.view.frame.width - 20, height: 350)
        ratingVC.delegate=self
        self.presentPopupViewController(ratingVC, animationType: MJPopupViewAnimationFade)
        
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
class rateCell: UITableViewCell {
    @IBOutlet var imgView: UIImageView!
}
