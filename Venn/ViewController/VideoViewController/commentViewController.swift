//
//  commentViewController.swift
//  Venn
//
//  Created by Hetal Govani on 28/11/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit

class commentViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate
{
    @IBOutlet var tblObj : UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        let button1 = UIButton(type: UIButtonType.custom)
        button1.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        button1.frame=CGRect.init(x: 0, y: 0, width: 30, height: 40)
        let barButton1 = UIBarButtonItem(customView: button1)
        self.navigationItem.leftBarButtonItem = barButton1
        
        let btnright1 = UIButton(type: UIButtonType.custom)
        btnright1.setImage(#imageLiteral(resourceName: "broadcast_light"), for: UIControlState.normal)
        //        button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        btnright1.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barbtnright1 = UIBarButtonItem(customView: btnright1)
        
        let btnright2 = UIButton(type: UIButtonType.custom)
        btnright2.setImage(#imageLiteral(resourceName: "search"), for: UIControlState.normal)
        //        button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        btnright2.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barbtnright2 = UIBarButtonItem(customView: btnright2)
        
        let btnright3 = UIButton(type: UIButtonType.custom)
        btnright3.setImage(#imageLiteral(resourceName: "student_pink"), for: UIControlState.normal)
        //        button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        btnright3.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barbtnright3 = UIBarButtonItem(customView: btnright3)
        self.navigationItem.rightBarButtonItems = [barbtnright3,barbtnright2,barbtnright1]
        
        self.navigationController?.navigationBar.isHidden = false
     //   tblObj.rowHeight = UITableViewAutomaticDimension
//        tblObj.estimatedRowHeight = 44
    }
  
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }


    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let attrText = NSAttributedString.init(string: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec")
        
        return attrText.height(withConstrainedWidth: self.view.frame.width - 80) + 80
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        
        //-------------------
       
        if indexPath.row % 2 != 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellOther", for: indexPath) as! otherChatCell
            cell.selectionStyle = .none
            
            cell.txtComment.attributedText = NSMutableAttributedString.init(string: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec", attributes:[NSAttributedStringKey.foregroundColor : UIColor.white,NSAttributedStringKey.font : UIFont.init(name: "Avenir-Roman", size: 16)!])
            cell.imgBubble?.image = #imageLiteral(resourceName: "bubble_incoming")
            cell.txtComment.textContainerInset = UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15)

            cell.txtComment.adjustsFontForContentSizeCategory = false
            let tSize = cell.txtComment.sizeThatFits(CGSize(width:self.view.frame.width - 80  , height: CGFloat.greatestFiniteMagnitude))
            cell.commentHeight.constant = tSize.height
            cell.commentWidth.constant = tSize.width
            cell.imgBubble = UIImageView(frame: CGRect.init(x:0.0, y:0.0, width:tSize.width, height: tSize.height))
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellMy", for: indexPath) as! myChatCell
            cell.selectionStyle = .none
//            stringNumeroMutable = NSMutableAttributedString(string: stringNumero as! String, attributes: [NSFontAttributeName: UIFont(name: "Noteworthy-Light", size: 9)!,

            cell.txtComment.attributedText = NSMutableAttributedString.init(string: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec", attributes:[NSAttributedStringKey.foregroundColor : UIColor.white,NSAttributedStringKey.font : UIFont.init(name: "Avenir-Roman", size: 16)!])
            cell.imgBubble?.image = #imageLiteral(resourceName: "bubble_outgoing")
            cell.txtComment.textContainerInset = UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15)

//            cell.txtComment.font = UIFont.init(name: "Avenir-Roman", size: 16)
            cell.txtComment.adjustsFontForContentSizeCategory = false
            let tSize = cell.txtComment.sizeThatFits(CGSize(width:self.view.frame.width - 80  , height: CGFloat.greatestFiniteMagnitude))
            cell.commentHeight.constant = tSize.height
            cell.commentWidth.constant = tSize.width
            cell.imgBubble = UIImageView(frame: CGRect.init(x:0.0, y:0.0, width:tSize.width, height: tSize.height))
            
            return cell
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
class myChatCell : UITableViewCell
{
    @IBOutlet var txtComment : UITextView!
    @IBOutlet var imgBubble : UIImageView!

    @IBOutlet var commentHeight :NSLayoutConstraint!
    @IBOutlet var commentWidth :NSLayoutConstraint!
}
class otherChatCell: UITableViewCell {
    @IBOutlet var txtComment : UITextView!
    @IBOutlet var imgBubble : UIImageView!

    @IBOutlet var commentHeight :NSLayoutConstraint!
    @IBOutlet var commentWidth :NSLayoutConstraint!
}

extension NSAttributedString {
    func height(withConstrainedWidth width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.width)
    }
}
extension String {
    func height(withConstrainedWidth width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstraintedHeight height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)], context: nil)
        
        return ceil(boundingBox.width)
    }
}
