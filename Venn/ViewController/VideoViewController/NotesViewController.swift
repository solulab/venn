//
//  NotesViewController.swift
//  Venn
//
//  Created by Hetal Govani on 27/11/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit

class NotesViewController: UIViewController
{
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        
        let button1 = UIButton(type: UIButtonType.custom)
        button1.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        button1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        button1.frame=CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barButton1 = UIBarButtonItem(customView: button1)
        self.navigationItem.leftBarButtonItem = barButton1
        
        let btnright1 = UIButton(type: UIButtonType.custom)
        btnright1.setTitle("Done", for: .normal)
        btnright1.setTitleColor(GRAYCOLOR, for: .normal)
        btnright1.addTarget(self, action:#selector(btnBackPress(sender:)), for: UIControlEvents.touchUpInside)
        btnright1.frame=CGRect.init(x: 0, y: 0, width: 50, height: 30)
        let barbtnright1 = UIBarButtonItem(customView: btnright1)
        self.navigationItem.rightBarButtonItem = barbtnright1
    }
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
