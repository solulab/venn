//
//  VideoChatViewController.swift
//  Venn
//
//  Created by Hetal Govani on 23/11/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit

class VideoChatViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    var cellcount : CGFloat = 0
    var cellWidth : CGFloat = 0
    var cellSpecing : CGFloat = 0

    @IBOutlet var viewPopover : UIView!
    @IBOutlet var btnDocument : UIButton!
    @IBOutlet var imgDocument : UIImageView!
    @IBOutlet var lblDocument : UILabel!
    
    @IBOutlet var btnCamera : UIButton!
    @IBOutlet var imgCamera : UIImageView!
    @IBOutlet var lblCamera : UILabel!

    @IBOutlet var btnPhotos : UIButton!
    @IBOutlet var imgPhotos : UIImageView!
    @IBOutlet var lblPhotos : UILabel!

    @IBOutlet var btnAudio : UIButton!
    @IBOutlet var imgAudio : UIImageView!
    @IBOutlet var lblAudio : UILabel!

    @IBOutlet var btnVideo : UIButton!
    @IBOutlet var imgVideo : UIImageView!
    @IBOutlet var lblVideo : UILabel!

    override func viewDidLoad()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.enableAllOrientation = true
        super.viewDidLoad()
        
        let value = UIInterfaceOrientation.landscapeLeft.rawValue;
        UIDevice.current.setValue(value, forKey: "orientation")
        cellcount = 8
        cellWidth = 60
        cellSpecing = 10
        
        viewPopover.isHidden = true
        
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapBlurButton(_:)))
//        self.view.addGestureRecognizer(tapGesture)
    }
//    @objc func tapBlurButton(_ sender: UITapGestureRecognizer) {
//        viewPopover.isHidden = TUREAD
//    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true

    }
    @IBAction func btnDocumentPress(sender:UIButton)
    {
        imgDocument.image = #imageLiteral(resourceName: "document_selected")
        imgCamera.image = #imageLiteral(resourceName: "camera")
        imgPhotos.image = #imageLiteral(resourceName: "photos")
        imgAudio.image = #imageLiteral(resourceName: "audio")
        imgVideo.image = #imageLiteral(resourceName: "video")
        
        lblDocument.textColor = PINKCOLOR
        lblCamera.textColor = GRAYCOLOR
        lblPhotos.textColor = GRAYCOLOR
        lblAudio.textColor = GRAYCOLOR
        lblVideo.textColor = GRAYCOLOR

    }
    @IBAction func btnCameraPress(sender:UIButton)
    {
        imgDocument.image = #imageLiteral(resourceName: "document")
        imgCamera.image = #imageLiteral(resourceName: "camera_selected")
        imgPhotos.image = #imageLiteral(resourceName: "photos")
        imgAudio.image = #imageLiteral(resourceName: "audio")
        imgVideo.image = #imageLiteral(resourceName: "video")
        
        lblDocument.textColor = GRAYCOLOR
        lblCamera.textColor = PINKCOLOR
        lblPhotos.textColor = GRAYCOLOR
        lblAudio.textColor = GRAYCOLOR
        lblVideo.textColor = GRAYCOLOR
        
    }
    @IBAction func btnPhotosPress(sender:UIButton)
    {
        imgDocument.image = #imageLiteral(resourceName: "document")
        imgCamera.image = #imageLiteral(resourceName: "camera")
        imgPhotos.image = #imageLiteral(resourceName: "photos_selected")
        imgAudio.image = #imageLiteral(resourceName: "audio")
        imgVideo.image = #imageLiteral(resourceName: "video")
        
        lblDocument.textColor = GRAYCOLOR
        lblCamera.textColor = GRAYCOLOR
        lblPhotos.textColor = PINKCOLOR
        lblAudio.textColor = GRAYCOLOR
        lblVideo.textColor = GRAYCOLOR
    }
    @IBAction func btnAudioPress(sender:UIButton)
    {
        imgDocument.image = #imageLiteral(resourceName: "document")
        imgCamera.image = #imageLiteral(resourceName: "camera")
        imgPhotos.image = #imageLiteral(resourceName: "photos")
        imgAudio.image = #imageLiteral(resourceName: "audio_selected")
        imgVideo.image = #imageLiteral(resourceName: "video")
        
        lblDocument.textColor = GRAYCOLOR
        lblCamera.textColor = GRAYCOLOR
        lblPhotos.textColor = GRAYCOLOR
        lblAudio.textColor = PINKCOLOR
        lblVideo.textColor = GRAYCOLOR
        
    }
    @IBAction func btnVideoPress(sender:UIButton)
    {
        imgDocument.image = #imageLiteral(resourceName: "document")
        imgCamera.image = #imageLiteral(resourceName: "camera")
        imgPhotos.image = #imageLiteral(resourceName: "photos")
        imgAudio.image = #imageLiteral(resourceName: "audio")
        imgVideo.image = #imageLiteral(resourceName: "video_selected")
        
        lblDocument.textColor = GRAYCOLOR
        lblCamera.textColor = GRAYCOLOR
        lblPhotos.textColor = GRAYCOLOR
        lblAudio.textColor = GRAYCOLOR
        lblVideo.textColor = PINKCOLOR
        
    }
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! chatButtonCell
        
        if indexPath.row == 0
        {
            cell.imgView.image = #imageLiteral(resourceName: "endcall")
        }
        else if indexPath.row == 1
        {
            cell.imgView.image = #imageLiteral(resourceName: "flip")
        }
        else if indexPath.row == 2
        {
            cell.imgView.image = #imageLiteral(resourceName: "mice")
        }
        else if indexPath.row == 3
        {
            cell.imgView.image = #imageLiteral(resourceName: "comment")
        }
        else if indexPath.row == 4
        {
            cell.imgView.image = #imageLiteral(resourceName: "attachment")
        }
        else if indexPath.row == 5
        {
            cell.imgView.image = #imageLiteral(resourceName: "notes")
        }
        else if indexPath.row == 6
        {
            cell.imgView.image = #imageLiteral(resourceName: "broadcast")
        }
        else
        {
            cell.imgView.image = #imageLiteral(resourceName: "add")
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        
        let totalCellWidth = cellWidth * cellcount
        let totalSpacingWidth = cellSpecing * (cellcount - 1)
        
        let leftInset = ((self.view.frame.width - 40) - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        
        return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let cell = collectionView.cellForItem(at: indexPath) as! chatButtonCell

//        collectionView.reloadData()
    
        if indexPath.row == 1
        {
            cell.imgView.image = #imageLiteral(resourceName: "flip_selected")
            viewPopover.isHidden = true
        }
        else if indexPath.row == 2
        {
            cell.imgView.image = #imageLiteral(resourceName: "mice_selected")
            viewPopover.isHidden = true
        }
        else if indexPath.row == 3
        {
            cell.imgView.image = #imageLiteral(resourceName: "comment_selected")
            viewPopover.isHidden = true
            let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "commentViewController") as! commentViewController
            self.navigationController?.pushViewController(viewObj, animated: true)
        }
        else if indexPath.row == 4
        {
            cell.imgView.image = #imageLiteral(resourceName: "attachment_selected")
            let attr = collectionView.layoutAttributesForItem(at: indexPath)!
            viewPopover.frame = CGRect.init(x: attr.frame.width/2, y: viewPopover.frame.origin.y, width: viewPopover.frame.width, height: viewPopover.frame.height)
            viewPopover.isHidden = false
        }
        else if indexPath.row == 5
        {
            cell.imgView.image = #imageLiteral(resourceName: "notes_selected")
            viewPopover.isHidden = true
            
            let viewObj = self.storyboard?.instantiateViewController(withIdentifier: "NotesViewController") as! NotesViewController
            self.navigationController?.pushViewController(viewObj, animated: true)
        }
        else if indexPath.row == 6
        {
            cell.imgView.image = #imageLiteral(resourceName: "broadcast_selected")
            viewPopover.isHidden = true
        }
        else if indexPath.row == 7
        {
            cell.imgView.image = #imageLiteral(resourceName: "add_selected")
            viewPopover.isHidden = true
        }
    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! chatButtonCell
        
        if indexPath.row == 1
        {
            cell.imgView.image = #imageLiteral(resourceName: "flip")
        }
        else if indexPath.row == 2
        {
            cell.imgView.image = #imageLiteral(resourceName: "mice")
        }
        else if indexPath.row == 3
        {
            cell.imgView.image = #imageLiteral(resourceName: "comment")
        }
        else if indexPath.row == 4
        {
            cell.imgView.image = #imageLiteral(resourceName: "attachment")
        }
        else if indexPath.row == 5
        {
            cell.imgView.image = #imageLiteral(resourceName: "notes")
        }
        else if indexPath.row == 6
        {
            cell.imgView.image = #imageLiteral(resourceName: "broadcast")
        }
        else
        {
            cell.imgView.image = #imageLiteral(resourceName: "add")
        }
    }
}
class chatButtonCell : UICollectionViewCell
{
    @IBOutlet var imgView : UIImageView!
}
