//
//  RateViewController.swift
//  Venn
//
//  Created by Hetal Govani on 14/12/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
protocol RateUsDelegate
{
    func SubmitButtonClicked(_ secondDetailViewController: RateViewController)
    func DismissButtonClicked(_ secondDetailViewController: RateViewController)
}
class RateViewController: UIViewController {

     var delegate: RateUsDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    @IBAction func btnSubmitPress(sender:UIButton)
    {
        delegate?.SubmitButtonClicked(self)
    }
    @IBAction func btnDismissPress(sender:UIButton)
    {
        delegate?.DismissButtonClicked(self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
