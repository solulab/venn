//
//  LoginRegisterTextField.swift
//
//  Created by Ankit on 3/31/16.
//  Copyright © 2016 Ankit. All rights reserved.
//

import UIKit

@IBDesignable
class customTextField: UITextField {

    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    @IBInspectable var placeHolderColor: UIColor?
        {
        didSet
        {
            self.setValue(placeHolderColor, forKeyPath:"_placeholderLabel.textColor")
        }
    }
    @IBInspectable var paddingLeft: CGFloat = 0 {
        didSet {
            let paddingView = UIView(frame:CGRect.init(x: 0, y: 0, width: paddingLeft, height: self.frame.size.height))
            self.leftView=paddingView;
            self.leftViewMode = UITextFieldViewMode.always
        }
    }
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect)
    {
        // Drawing code
        
  //      let lineView = UIView(frame: CGRect.init(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: 1))
  //      lineView.backgroundColor=UIColor(red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1)
  //      self.addSubview(lineView)
    }
}
@IBDesignable
class customBottomLineTextField: UITextField {
    
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    @IBInspectable var placeHolderColor: UIColor?
        {
        didSet
        {
            self.setValue(placeHolderColor, forKeyPath:"_placeholderLabel.textColor")
        }
    }
//    @IBInspectable var bottomBorderColor: UIColor? {
//        get {
//            return self.bottomBorderColor
//        }
//        set {
//            self.borderStyle = UITextBorderStyle.none;
//            let border = CALayer()
//            let width = CGFloat(0.5)
//            border.borderColor = newValue?.cgColor
//            border.frame = CGRect(x: 0, y: self.frame.size.height - width,   width:  self.frame.size.width, height: self.frame.size.height)
//
//            border.borderWidth = 0.5
//            self.layer.addSublayer(border)
//            self.layer.masksToBounds = true
//
//        }
//    }
   
    @IBInspectable var paddingLeft: CGFloat = 0 {
        didSet {
            let paddingView = UIView(frame:CGRect.init(x: 0, y: 0, width: paddingLeft, height: self.frame.size.height))
            self.leftView=paddingView;
            self.leftViewMode = UITextFieldViewMode.always
        }
    }
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect)
    {
        // Drawing code
        let lineView = UIView(frame: CGRect.init(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: 0.5))
        lineView.backgroundColor=borderColor
        self.addSubview(lineView)
    }
}


@IBDesignable
class customTextView: UITextView {
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect)
    {
        // Drawing code
        
//        let lineView = UIView(frame: CGRect.init(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: 1))
//        lineView.backgroundColor=UIColor(red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1)
//        self.addSubview(lineView)
    }
}
